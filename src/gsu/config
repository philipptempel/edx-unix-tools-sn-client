#!/bin/bash
# Copyright (C) 2006 Andre Noll
# Licensed under the LGPL, version 3. See COPYING and COPYING.LESSER.

# Syntactically check the gsu_options array for errors and parse the config
# file.
gsu_check_options()
{
	local i conf="${gsu_config_file:=${HOME:-}/.$gsu_name.rc}" val orig_val
	local name option_type default_value required description help_text

	for ((i=0; i < ${#gsu_options[@]}; i++)); do
		eval "${gsu_options[$i]}"
		eval val='"'\${${name}:-}'"'
		eval orig_${gsu_config_var_prefix}_$name='"'${val}'"'
	done

	[[ -r "$conf" ]] && source "$conf"

	for ((i=0; i < ${#gsu_options[@]}; i++)); do
		name=
		option_type=
		default_value=
		required=
		description=
		help_text=

		eval "${gsu_options[$i]}"

		# Check name. It must be non-empty and consist of [a-zA-Z_0-9]
		# only.  Moreover it must start with [a-zA-Z].
		ret=-$E_GSU_BAD_CONFIG_VAR
		result="name: '$name'"
		[[ "$name" =~ ^[a-zA-Z][a-zA-Z_0123456789]*$ ]] || return

		eval orig_val='"'\$orig_${gsu_config_var_prefix}_$name'"'
		if [[ -z "$orig_val" ]]; then
			eval val='"'\${$name:-}'"'
		else
			val="$orig_val"
		fi
		case "$required" in
		true|yes)
			ret=-$E_GSU_NEED_VALUE
			result="$name"
			[[ -z "$val" ]] && return
			;;
		false|no)
			;;
		*)
			ret=-$E_GSU_BAD_BOOL
			result="required: $required, name: $name, val: $val"
			return
		esac

		eval ${gsu_config_var_prefix}_$name='"'\${val:="$default_value"}'"'
		# Check option type. ATM, only num and string are supported
		# Other types may be added without breaking compatibility
		case "$option_type" in
		string)
			;;
		num)
			gsu_is_a_number "$val"
			((ret < 0)) && return
			;;
		*)
			ret=-$E_GSU_BAD_OPTION_TYPE
			result="$name/$option_type"
			return
		esac
	done
	ret=$GSU_SUCCESS
}

# Call gsu_check_options(), die on errors.
gsu_check_options_or_die()
{
	gsu_check_options
	if ((ret < 0)); then
		gsu_err_msg
		exit 1
	fi
}

if [[ "$(type -t _gsu_setup)" != "function" ]]; then
	gsu_dir=${gsu_dir:-${BASH_SOURCE[0]%/*}}
	. "$gsu_dir/common" || exit 1
	_gsu_setup
fi
