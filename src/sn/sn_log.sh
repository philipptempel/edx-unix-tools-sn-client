#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_log()
##
## Show a list of existing posts
## 
## Usage: log
##
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # list all posts with its body trimmed to 80 chars
  if [ "$(wc -l < "posts.dat")" -le 1 ]; then
    echo 'No posts found'
  # get posts and sort such that the newest post is at the top
  else
    tail -n +2 "posts.dat" | sort --field-separator=',' --reverse --key=4,4 | awk -F',' -v len=80 '
    {print $2 " (" substr($1, 0, 8) " by " $5 " on " $4 ")"}  # `title` (`id` by `author` on `date`)
    {if (length($3) > len) print substr($3, 0, len-3) "..." ; else print $3}  # shortened `body`
    {print ""}' # new line
  fi
}
