#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_show()
##
## Show a specified post
##
## Usage: show title
## Usage: show hash
##
## Any additional arguments to this function are ignored.
##
## title:     Post title for which to search. Must be the full title.
## hash:      Post hash for which to search. Either a full hash or the 8-digit
##            short form 
##
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # require one argument (the post's title)
  gsu_check_arg_count $# 1 1
  ((ret < 0)) && return

  # post title for finding
  target="$1"

  # no post found?
  if ! post=$(_sn_get_post "$target"); then
    ret=-${E_NOPOSTFOUND}
    result="Aborting"
    return
  fi

  # read post data into variable
  postHash="$(echo -n "$post" | cut -d, -f1)"
  postTitle="$(echo -n "$post" | cut -d, -f2)"
  postBody="$(echo -n "$post" | cut -d, -f3)"
  postDate="$(echo -n "$post" | cut -d, -f4)"
  postAuthor="$(echo -n "$post" | cut -d, -f5)"
  postLikes="$(tail -n +2 "likes.dat" | awk -F',' -v h="${postHash}" '{if (h == $2) print $0}' | wc -l)"
  
  # output the post in a nicely formatted manner
  cat <<EOF
Title: "${postTitle}"
Author: "${postAuthor}"
Date: "${postDate}"
Hash: "${postHash}"
Likes: "${postLikes}"

"${postBody}"
EOF
}
