#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_like()
##
## Like a specified post
## 
## Usage: like post
##
## post:      Hash of post to like
##
## Any additional arguments to this function are ignored.
##
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # require one argument (post hash)
  gsu_check_arg_count $# 1 1
  ((ret < 0)) && return

  # post hash for liking
  target="${1}"

  # find post
  if ! post=$(_sn_get_post "$target"); then
    ret=-${E_NOPOSTFOUND}
    result="Aborting"
    return
  fi

  # post hash
  target="$(echo -n "$post" | cut -d, -f1)"
  # user name
  source="$(_sn_user_email)"

  # add like to list
  echo "${source},${target}" >> "likes.dat"

  # sort the list and write it to the file
  cat <<EOF > "likes.dat"
member,post
$(tail -n +2 "likes.dat" | sort --field-separator=',' --key=1 --key=2 | uniq)
EOF
  
  # add and commit changes (if any) are found
  if ! git diff-index --quiet HEAD -- "likes.dat"; then
    git add "likes.dat"
    git commit -m "Add like: ${source} liked post ${target}."
  fi
}
