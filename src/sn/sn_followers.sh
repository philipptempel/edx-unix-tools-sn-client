#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_followers()
##
## List all followers
##
## Usage: followers [user]
##
## Any additional arguments to this function are ignored.
##
## user:      Optional argument. Provide email address of a member, if you want
##            to see their followers
##
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # require one argument (the network name)
  gsu_check_arg_count $# 0 1
  ((ret < 0)) && return
  source="${1}"

  # no user name provided, so we will use the current user's name
  if [ -z "$source" ]; then
    source=$(_sn_user_email)
  fi

  # check user exists
  if ! _sn_validate_user "$source"; then
    ret=-${E_USERNOTFOUND}
    result="Aborting"
    return
  fi

  # find all lines where the follower matches `${source}`
  if targets=$(awk -F',' -v r="${source}" '$2==r {print $1}' "subscriptions.dat"); then
    for target in "${targets[@]}"; do
      echo "${target}"
    done
  fi
}
