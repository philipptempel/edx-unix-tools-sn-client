#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

_sn_message()
##
## Output a message to the user
##
## Usage: _sn_message level message
##
## level:     Log level oof the message. If the log level is less than the
##            active level of verbosity, it will be put out
## message:   Message to put out
{
  [ "${SN_VERBOSE}" -gt 0 ] && [ "${1}" -gt "${SN_VERBOSE}" ] && { shift; echo "$@"; }
}

_sn_database_available()
##
## Check if database is available
##
## Usage: _sn_database_available
##
## Checks if the needed "DAT" files are available
{
  # check we have all needed database files
  if [ ! -f "members.dat" ] | [ ! -f "posts.dat" ] | [ ! -f "subscriptions.dat" ] | [ ! -f "likes.dat" ]; then
    ret=-${E_DATABASECORRUPT}
    result="Aborting"

    return 1
  fi

  return 0
}

_sn_user_email()
##
## Get the current user's email
##
{
  echo -n "$(git config --get "user.email")"
}

_sn_user_name()
##
## Get the current user's name
##
{
  echo -n "$(git config --get "user.name")"
}

_sn_validate_user()
##
## Validate a user by email
##
## Usage: _sn_validate_user email
##
## email:     User's email address to find
{
  # count the number of lines the email address is grep'ed completey. if the
  # number of lines is equal to 1 (one), we have a user. so return 0, the
  # default success return code in bash
  _sn_message "${SN_LOGLEVEL_DEBUG}" "Looking for user ${1}"
  
  
  # if a line was found, grep returned a non-zero code
  if line="$(tail -n +2 "members.dat" | cut -d"," -f1 | grep -noi "^${1}$")"; then
    if [ "$(echo "${line}" | wc -l)" -eq 1 ]; then
      _sn_message "${SN_LOGLEVEL_DEBUG}" "Found user with matching line ${line}"
      # success
      return 0
    fi
  fi

  # return error indicator
  return "${E_USERNOTFOUND}"
}

_sn_get_user()
##
## Get a user entry
##
## Usage: _sn_get_user email
##
## email:     User's email address to find by
{
  # get email
  email="$1"
  
  # validate user
  if ! line=$(_sn_validate_user "${email}"); then
    return 1
  fi

  # return the line from the members database
  sed "$(($(tail -n +2 "members.dat" | cut -d"," -f1 | grep -noi "^${1}$" | cut -d':' -f1) + 1))q;d" "members.dat"

  return 0
}

_sn_get_post()
##
## Get a user entry
##
## Usage: _sn_get_user title
## Usage: _sn_get_user hash
##
## title:     Full title of the post to find
## hash       Full or 8-digit post to find
{
  # get title or hash
  titleOrHash="$1"

  # if not found by title, try finding by hash
  if ! line=$(tail -n +2 "posts.dat" | cut -d"," -f2 | grep -noi "^${titleOrHash}$"); then
    line=$(tail -n +2 "posts.dat" | cut -d"," -f1 | grep -noi "^${titleOrHash}")
  fi
  
  # if a line was found, grep returned a non-zero code
  if [ $? -eq 0 ] & [ "$(echo "${line}" | wc -l)" -eq 1 ]; then
    # get line number and advance by one to account for the header
    line=$(($(echo -n "${line}" | cut -d':' -f1) + 1))
    sed "${line}q;d" "posts.dat"
    # success
    return 0
  fi

  # return error indicator
  return 1
}

_sn_create_hash()
##
## Create a hash for a given text
##
## Usage: _sn_create_hash text
##
## text:    The text to create the hash for
{
  echo -n "${@}" | git hash-object --stdin
  return "$?"
}
