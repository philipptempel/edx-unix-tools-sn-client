#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_members()
##
## Show the network's members
##
## Usage: members
##
## Any additional arguments to this function are ignored
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # read the DAT file and parse in commonly known email format
  awk -F',' '{print $2",<"$1">"}' "members.dat" | column -t -s ","
}
