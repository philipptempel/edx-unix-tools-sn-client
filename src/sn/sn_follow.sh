#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_follow()
##
## Follow the posts of the specified member
##
## Usage: follow email
##
## Any additional arguments to this function are ignored.
##
## email:   Email address of the user you want to follow. See `sn members` to
##          for a list of email addresses.
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # require one argument (the followee's email)
  gsu_check_arg_count $# 1 1
  ((ret < 0)) && return

  # get target name
  target="$1"
  # get own name
  source="$(_sn_user_email)"

  # check user exists
  if ! _sn_validate_user "$target"; then
    ret=-${E_USERNOTFOUND}
    result="Aborting"
    return
  fi

  # check subscription does not exist
  if tail -n +2 "subscriptions.dat" | grep -i "^${source},${target}$" > /dev/null; then
    return
  fi

  # add subscription to the list
  echo "${source},${target}" >> "subscriptions.dat"

  # sort list and write it back into file
  cat <<EOF > "subscriptions.dat"
member,subscribed_to
$(tail -n +2 "subscriptions.dat" | sort --field-separator=',' --key=1 --key=2 | uniq)
EOF
  
  # add and commit changes (if any) are found
  if ! git diff-index --quiet HEAD -- "subscriptions.dat"; then
    git add "subscriptions.dat"
    git commit -m "Add subscription: ${source} subscribed to ${target}."
  fi
}
