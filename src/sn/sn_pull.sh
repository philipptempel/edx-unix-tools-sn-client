#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_pull()
##
## Pull in new posts and likes
##
## Usage: pull
##
## Any additional arguments to this function passed down to the `git` call
##
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # just use git to pull data (that should be fine, right?)
  git pull "$@"
}
