#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_join()
##
## Join an existing social network by cloning it to your local system
##
## Usage: join name
##
## Any additional arguments to this function passed down to the `git` call
##
## name:  name of the social network to join. right now, there only exists one
##        such network named `tudelft`
##
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # require one argument (the network name)
  gsu_check_arg_count $# 1 1
  ((ret < 0)) && return

  # get network name
  target="$1"

  # clone network
  if git clone https://gitlab.com/philipptempel/edx-sn-"${target}".git; then
    ret=-${E_CLONEFAILED}
    result="Aborting"
    return
  fi

  # change into the network and add us as a member (if we aren't already)
  if ! cd "edx-sn-${target}"; then
    ret=-${E_CLONEFAILED}
    result="Aborting"
    return
  fi

  # obtain data for new user
  userName=$(_sn_user_name)
  userEmail=$(_sn_user_email)

  # write new member to dat file
  echo "${userEmail},${userName}" >> "members.dat"

  # sort list and write it back into file
  cat <<EOF > "members.dat"
email,name
$(tail -n +2 "members.dat" | sort | uniq --field-separator=',' --key=1,1)
EOF
  
  # add and commit changes (if any) are found
  if ! git diff-index --quiet HEAD -- "members.dat"; then
    git add "members.dat"
    git commit -m "User ${userName} <${userEmail}> joined the network."
  fi
}
