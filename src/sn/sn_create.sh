#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_create()
##
## Create a new social network
##
## Usage: create
##
## Any additional arguments to this function are passed down to the `git` call.
##
{
  # check for local `.git` directory which would correspond to the networking
  # having been created already
  if [ -d ".git" ]; then
    ret=-${E_ALREADYCREATED}
    result="Aborting"
    return
  fi

  # initialize a git repository in the current working directory
  git init "$@"

  # create all needed files as empty DAT files (basically just a CSV)
  # initially, no posts
  echo "hash,title,body,date,author" > "posts.dat"
  # initially, no likes
  echo "member,post" > "likes.dat"
  # initially no subscriptions
  echo "member,subscribed_to" > "subscriptions.dat"
  # creates the members list with us as the first network member
  userName=$(_sn_user_name)
  userEmail=$(_sn_user_email)
  echo "email,name" > "members.dat"
  echo "${userEmail},${userName}" >> "members.dat"

  # add all files
  git add members.dat posts.dat subscriptions.dat likes.dat

  # commit all files
  git commit -m "Initial commit: create network"

  # in debug mode, also add the remote right away (I'm a lazy developer)
  if [ "$SN_DEBUG" ]; then
    git remote add origin https://gitlab.com/philipptempel/edx-sn-tudelft.git
  fi
}
