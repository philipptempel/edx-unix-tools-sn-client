#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_post()
##
## Post a new story
##
## Usage: post "title" "body"
##
## Any additional arguments to this function are ignored
##
## title:   Your post's title. Make sure you properly warp it in quotes.
## body:    The body's post. Can be multiline if properly wrapped in quotes.
##
{
  # require one argument (post title and post body)
  gsu_check_arg_count $# 2 2
  ((ret < 0)) && return

  # post title
  postTitle="$1"
  # post body
  postBody="$2"
  # post author
  postAuthor="$(_sn_user_email)"
  # post date
  postDate="$(date -u '+%Y%m%dT%H%M%SZ')"
  # post identifier
  postHash="$(_sn_create_hash "${postTitle}" "${postBody}" "${postAuthor}" "${postDate}")"

  # just append a properly formatted line to the DAT file of posts
  cat <<EOF >> "posts.dat"
${postHash},${postTitle},${postBody},${postDate},${postAuthor}
EOF
  
  # add and commit changes (if any) are found
  if ! git diff-index --quiet HEAD -- "posts.dat"; then
    git add "posts.dat"
    git commit -m "Add post: ${postTitle} by ${postAuthor}."
  fi
}
