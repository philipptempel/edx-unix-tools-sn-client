#!/usr/bin/env bash
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

if [ -z "${SN_INIT+x}" ]; then
  echo >&2 "Script must not be run directly but only through 'sn.sh'.  Aborting."
  exit 1
fi

main_sn_unfollow()
##
## Unfollow the posts of the specified member
##
## Usage: unfollow email
##
## Any additional arguments to this function are ignored.
##
## email:   Email address of the user you want to unfollow. See `sn members` to
##          for a list of email addresses.
{
  # check "database" is up and running
  if ! _sn_database_available; then
    return
  fi

  # require one argument (the network name)
  gsu_check_arg_count $# 1 1
  ((ret < 0)) && return

  # get target name
  target="$1"
  # get own name
  source=$(_sn_user_email)

  # remove the entry from the list and write a new subscriptions file
  cat <<EOF > "subscriptions.dat"
member,subscribed_to
$(tail -n +2 "subscriptions.dat" | sed -E "/^${source},${target}$/d; /^[[:space:]]*$/d; /^\s*$/d")
EOF
  
  # add and commit changes (if any) are found
  if ! git diff-index --quiet HEAD -- "subscriptions.dat"; then
    git add "subscriptions.dat"
    git commit -m "Remove subscription: ${source} unsubscribed from ${target}."
  fi
}
