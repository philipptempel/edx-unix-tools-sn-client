#!/usr/bin/env bash
# shellcheck source=./src/sn/bootstrap.sh
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

main_boostrap()
##
## Main callback
##
## Usage: main_boostrap
##
{
  gsu_errors='
    E_ALREADYCREATED  Network already created in local directory.
    E_DATABASECORRUPT Local network database corrupt or missing.
    E_CLONEFAILED     Failure to join network; cloning failed.
    E_NOPOSTFOUND     Post not found.
    E_USERNOTFOUND    User not found.
  '

  gsu_banner_txt="edX Unix Tools Social Network"

  # global variables unique of the script
  SN_DEBUG=false
  SN_VERBOSE=0
  SN_INIT=true

  # logging level thresholds
  SN_LOGLEVEL_CRITICAL=50
  SN_LOGLEVEL_ERROR=40
  SN_LOGLEVEL_WARNING=30
  SN_LOGLEVEL_INFO=20
  SN_LOGLEVEL_DEBUG=10
  
  _sn_check_requirements
  _sn_include
}

_sn_check_requirements()
##
## Check system meets all requirements to run `sn`.
##
## Usage: _sn_check_requirements
##
{
  command -v git >/dev/null 2>&1 || { echo >&2 "I require \`git\` but it's not installed.  Aborting."; exit 1; }
}


_sn_include()
##
## Include all subcommands that can be found.
##
## Usage: _sn_include
##
## Subcommands are defined in standalone files `sn_*` and must contain a
## name-matching function to be picked up by `GSU`
##
{
  for cmd in "${0%/*}"/src/sn/sn_*.sh; do
    . "${cmd}" || exit 1
  done
}

# execute main function
main_boostrap
