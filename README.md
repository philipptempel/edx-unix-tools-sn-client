# README

## Installation

If not already so, make the `sn.sh` script executable i.e.

```bash
$ chmod +x sn.sh
```

If you are one of the lazy people like me and don't want to type `sn.sh` all the time but rather just `sn`, then you will have to alias the command.
Maybe this code snippet works for you

```bash
$ alias sn="${PWD}/sn.sh"
```

In addition, ensure your `git` client is correctly set up and has values for both `user.name` and `user.email`.
If these are not set, you will run into troubles.
Read their values by

```bash
$ git config --global --get "user.name"
$ git config --global --get "user.email"
```

and possibly set them if they are empty via

```bash
$ git config --global --set "user.name "John Doe"
$ git config --global --set "user.email "jd@example.com"
```

## Usage

Just run

```bash
$ sn
```

and see which commands are available, what they do, what inputs they require, or what arguments they take.

## Documentation

To see which commands are available, just run

```bash
$ sn
```

or

```bash
$ sn help
```

To read more about each command, just run ```sn help [cmd]``` e.g.,

```bash
$ sn help create
```

This will tell you what the command does and list its arguments.

Be sure to also check out the man pages using

```bash
$ sn man
```

## Bug Reports

If you found a bug, which is very likely as this piece of software was rather written as proof of concept and a quick topdown mockup, you are very welcome to fix the bugs.

1. Fork the repo https://gitlab.com/philipptempel/edx-unix-tools-sn
1. Fix the changes
1. Add yourself to the contributors list
1. Create a merge request
1. Wait for the request to be merged and then send me a beer

## Testing

All tests are run manually.
There is absolutely no support for a testing framework since a) I don't know how to do that in bash, nor b) does it seem that any good unit testing framework exists.
See also this post on medium.com: https://medium.com/wemake-services/testing-bash-applications-85512e7fe2de

### ShellCheck

Shellcheck runs currently with a few minor issues that I don't know how to fix.
If you run shellcheck locally, make sure you are running on at least version `0.7.1`.
Otherwise, you might be getting other results than I am.

Currently, the following list contains shellcheck codes that are found but ignored in fixing

    * SC2068
    * SC2167

## License

Apache License 2.0


## Contributors

Philipp Tempel <p.tempel@tudelft.nl>
