#!/usr/bin/env bash
# shellcheck source=./sn.sh
# shellcheck disable=SC2034  # Unused variables left for readability
# shellcheck disable=SC2154  # Variable is referenced but not assigned.
# shellcheck disable=SC2167  # This parent loop has its index variable overridden.

################################################################################
# SOCIAL NETWORK
#
# This is the social network submission by Philipp Tempel <p.tempel@tudelft.nl>
# for the edX course 'Unix Tools: Data, Software and Production Engineering'
#
# Author: Philipp Tempel
# Copyright: 2020
# License: Apache License 2.0
################################################################################

# don't run glob loops if there are no files
shopt -s nullglob

# include our bootstrap script which checks requirements and also includes
# necessary files
. "${0%/*}/src/sn/bootstrap.sh" || exit 1

# include the `global subcommand` package for handling of argument parsing and
# sub command execution
. "${0%/*}/src/gsu/subcommand" || exit 1
. "${0%/*}/src/gsu/config" || exit 1

################################################################################
# registration and documentation of functions here
# 
# we opt for no automagic inspection since this makes code more delicate to
# handle and also because I don't know of any good inspection patterns in bash.
# there are awesome ones in Python, PHP, MATLAB, Perl, and even C++, but bash is
# a little limited--to my extent of knowledge
#
# to register a new function, simply add it here in the correct alphabetic order
# and implement its callback. the policy is to keep the functions here as short
# as possible i.e., basically, all methods here should be wrapper functions to
# external code where possible. external files are automatically included if
# they match the naming convention `sn_*.sh` (this happens in `bootstrap.sh`)

com_create()
##
## Create a new social network
##
## Usage: create
##
## Any additional arguments to this function passed down to the `git` call.
##
{
  main_sn_create "$@"
}


com_follow()
##
## Follow the posts of the specified member
##
## Usage: follow email
##
## Any additional arguments to this function are ignored.
##
## email:   Email address of the user you want to follow. See `sn members` to
##          for a list of email addresses.
{
  main_sn_follow "$@"
}


com_following()
##
## List all people a user is following
##
## Usage: following [user]
##
## Any additional arguments to this function are ignored.
##
## user:      Optional argument. Provide email address of a member, if you want
##            to see who they are following
##
{
  main_sn_following "$@"
}


com_followers()
##
## List all people following a user
##
## Usage: followers [user]
##
## Any additional arguments to this function are ignored.
##
## user:      Optional argument. Provide email address of a member, if you want
##            to see their followers
##
{
  main_sn_followers "$@"
}


com_join()
##
## Join an existing social network by cloning it to your local system
##
## Usage: join name
##
## Any additional arguments to this function passed down to the `git` call.
##
## name:  name of the social network to join. right now, there only exists one
##        such network named `tudelft`.
##
{
  main_sn_join "$@"
}


com_like()
##
## Like a specified post
## 
## Usage: like post
##
## post:      Hash of post to like
##
## Any additional arguments to this function are ignored.
##
{
  main_sn_like "$@"
}


com_log()
##
## Show a list of existing posts
## 
## Usage: log
##
{
  main_sn_log "$@"
}


com_members()
##
## Show the network's members
##
## Usage: members
##
## Any additional arguments to this function are ignored.
##
{
  main_sn_members "$@"
}


com_post()
##
## Post a new story
##
## Usage: post "title" "body"
##
## Any additional arguments to this function are ignored
##
## title:   Your post's title. Make sure you properly warp it in quotes.
## body:    The body's post. Can be multiline if properly wrapped in quotes.
##
{
  main_sn_post "$@"
}


com_pull()
##
## Pull in new posts and likes
##
## Usage: pull
##
## Any additional arguments to this function passed down to the `git` call.
##
{
  main_sn_pull "$@"
}


com_push()
##
## Push locally made changes back to the server
##
## Usage: push
##
## Any additional arguments to this function passed down to the `git` call.
##
{
  main_sn_push "$@"
}


com_show()
##
## Show a specified post
##
## Usage: show title
## Usage: show hash
##
## Any additional arguments to this function are ignored.
##
## title:     Post title for which to search. Must be the full title.
## hash:      Post hash for which to search. Either a full hash or the 8-digit
##            short form 
##
{
  main_sn_show "$@"
}


com_unfollow()
##
## Unfollow the posts of the specified member
##
## Usage: unfollow email
##
## Any additional arguments to this function are ignored.
##
## email:   Email address of the user you want to unfollow. See `sn members` to
##          for a list of email addresses.
{
  main_sn_unfollow "$@"
}

# END registration and documentation of functions here
################################################################################

# kick off script by letting the `global subcommand` handle everything form here
# on
gsu "$@"
